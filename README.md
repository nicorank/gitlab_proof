# Keyoxide Proof

[Verifying my OpenPGP key: openpgp4fpr:3F9B676D85844D4F354BD73FCFA063367B930ED6]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs
